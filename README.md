## Prerequiries

- PHP 7
- Composer
- MySQL

## Installation

- Clone the repository

### Docker pre-installation

- Enter into docker folder `cd ./docker`
- Build application `docker-compose build`
- Run application adn detach from console `docker-compose up -d`
- Connect to the container `docker-compose exec php /bin/bash`
- Make www-data user be able to create files in storage dir `chown -R www-data:www-data /var/www/html/storage`

### Installation

- Installer back dependencies with `composer install`
- Copy file `.env.example` in `.env` and add following informations:
    - Database credentials (`DB_HOST`, `DB_PORT`, ...)
    - Application url (`APP_URL`). Either virtual host address if you configure one, either address form the command `php artisan serve`
    - Retrieve and set your own `FIXER_API_KEY` from `https://fixer.io/`
    - Retrieve and set your own `CURRENCY_LAYER_API_KEY` from `https://currencylayer.com/`
- If you use docker you need to set:
    - `DB_HOST` as `mysql`
    - `DB_DATABASE` as `laravel`
    - `DB_USER` as `root`
    - `DB_PASSWORD` as `secret`
- Generate application key with `php artisan key:generate`
- Generate JWT key with `php artisan jwt:secret`
- Launch migrations with `php artisan migrate --seed`.

If you did'nt set a virtual host, launch application with the `php artisan serve` command. By default, application will be served at `http://127.0.0.1:8000`

## Configuration

You can change the length of time (in minutes) that the token will be valid for by changin the `JWT_TTL` value in the `.env` file.

## Usage

Log with `user@test.com:secret`

If you want to change delivery cost you need to set up `ORDER_DELIVERY_COST` environment variable in euro cents. E.g. for 1 euro you need to set up the variable as `ORDER_DELIVERY_COST=100`
