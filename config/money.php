<?php

declare(strict_types=1);

return [
    'default' => [
        'currency' => env('MONEY_DEFAULT_CURRENCY', 'EUR')
    ]
];
