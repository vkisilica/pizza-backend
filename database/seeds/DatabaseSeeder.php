<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        \Pizza\Account\Models\User::create([
            'name' => 'User',
            'email' => 'user@test.com',
            'password' => bcrypt('secret'),
        ]);

        $this->call([
            \Pizza\Catalog\Seeders\CatalogSeeder::class,
            \Pizza\Order\Seeders\OrderSeeder::class,
        ]);
    }
}
