<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Traits;

use Money\Money;
use Pizza\SharedKernel\Services\MoneyConverter\AccountantInterface;

trait HasPriceAttribute
{
    /**
     * @return Money
     */
    public function getPriceAttribute(): Money
    {
        $accountant = app(AccountantInterface::class);
        return $accountant->formatMoney((int)$this->attributes[static::PRICE_COLUMN]);
    }

    /**
     * @param Money $value
     */
    public function setPriceAttribute(Money $value)
    {
        $this->attributes[static::PRICE_COLUMN] = $value->getAmount();
    }
}
