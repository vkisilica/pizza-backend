<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Http\Resources;

abstract class ResourceCollection extends \Illuminate\Http\Resources\Json\ResourceCollection
{

}
