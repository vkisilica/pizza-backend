<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Http\Resources;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class Resource extends \Illuminate\Http\Resources\Json\Resource
{
    public function toArray($request)
    {
        $resource = parent::toArray($request);
        $resource = $this->toIso8601String($resource);

        return $resource;
    }

    protected function toIso8601String(array $resource)
    {
        if (! $this->resource instanceof Model) {
            return $resource;
        }

        $dateColumns = $this->resource->getDates();

        foreach ($dateColumns as $column) {
            /** @var Carbon $value */
            $value = $this->resource->getAttributeValue($column);
            $resource[$column] = optional($value)->toIso8601String();
        }

        return $resource;
    }
}
