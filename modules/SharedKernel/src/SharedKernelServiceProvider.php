<?php

declare(strict_types=1);

namespace Pizza\SharedKernel;

use Illuminate\Support\ServiceProvider;
use Money\Currencies;
use Money\Currencies\ISOCurrencies;
use Money\Exchange;
use Money\Exchange\SwapExchange;
use Money\Formatter\DecimalMoneyFormatter;
use Money\MoneyFormatter;
use Pizza\SharedKernel\Services\MoneyConverter\Accountant;
use Pizza\SharedKernel\Services\MoneyConverter\AccountantInterface;
use Pizza\SharedKernel\Services\MoneyConverter\ConverterAdapter;
use Pizza\SharedKernel\Services\MoneyConverter\ConverterInterface;

class SharedKernelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            realpath(__DIR__.'/../config/money.php') => config_path('money.php'),
        ], 'config');
    }

    /** @inherit */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            realpath(__DIR__.'/../config/money.php'), 'money'
        );

        $this->app->singleton(AccountantInterface::class, Accountant::class);
        $this->app->singleton(ConverterInterface::class, ConverterAdapter::class);

        $this->app->singleton(Currencies::class, ISOCurrencies::class);
        $this->app->singleton(Exchange::class, SwapExchange::class);
        $this->app->singleton(MoneyFormatter::class, DecimalMoneyFormatter::class);
    }
}
