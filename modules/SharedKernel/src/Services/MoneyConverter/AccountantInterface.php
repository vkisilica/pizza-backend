<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Services\MoneyConverter;

use Money\Money;

/**
 * Interface AccountantInterface
 * @package Pizza\SharedKernel\Services\MoneyConverter
 */
interface AccountantInterface
{
    /**
     * @param int $amount
     * @return Money
     */
    public function formatMoney(int $amount): Money;
}
