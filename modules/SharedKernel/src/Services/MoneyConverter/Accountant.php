<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Services\MoneyConverter;

use \InvalidArgumentException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\Config\Repository as Config;
use Money\Currency;
use Money\Money;
use Pizza\Account\Models\User;

/**
 * Class Accountant
 * @package Pizza\SharedKernel\Services\MoneyConverter
 */
class Accountant implements AccountantInterface
{
    /**
     * @var ConverterInterface
     */
    private $converter;
    /**
     * @var Guard
     */
    private $guard;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var Config
     */
    private $config;

    /**
     * Accountant constructor.
     * @param ConverterInterface $converter
     * @param Guard $guard
     * @param Session $session
     * @param Config $config
     */
    public function __construct(ConverterInterface $converter, Guard $guard, Session $session, Config $config)
    {
        $this->converter = $converter;
        $this->guard = $guard;
        $this->session = $session;
        $this->config = $config;
    }

    /**
     * @param int $amount
     * @return Money
     */
    public function formatMoney(int $amount): Money
    {
        if ($this->guard->check()) {
            /** @var User $user */
            $user = $this->guard->user();
            $userCurrency = $user->getAttributeValue(User::CURRENCY_COLUMN);
        } else {
            $userCurrency = $this->session->get('user.currency');
        }

        $defaultMoney = new Money($amount, new Currency($this->config->get('money.default.currency')));
        switch ($userCurrency) {
            case 'EUR':
                $money = $this->converter->convert($defaultMoney, new Currency('EUR'));
                break;
            case 'USD':
                $money = $this->converter->convert($defaultMoney, new Currency('USD'));
                break;
            case null:
                $money = $defaultMoney;
                break;
            default:
                throw new InvalidArgumentException(sprintf('Invalid currency %s', $userCurrency));
        }

        return $money;
    }
}
