<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Services\MoneyConverter;

use Money\Converter;
use Money\Currency;
use Money\Money;

/**
 * Class ConverterAdapter
 * @package Pizza\SharedKernel\Services\MoneyConverter
 */
class ConverterAdapter implements ConverterInterface
{
    /**
     * @var Converter
     */
    private $converter;

    /**
     * ConverterAdapter constructor.
     * @param Converter $converter
     */
    public function __construct(Converter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * @param Money $money
     * @param Currency $counterCurrency
     * @param int $roundingMode
     * @return Money
     */
    public function convert(Money $money, Currency $counterCurrency, $roundingMode = Money::ROUND_HALF_UP): Money
    {
        return $this->converter->convert($money, $counterCurrency, $roundingMode);
    }
}
