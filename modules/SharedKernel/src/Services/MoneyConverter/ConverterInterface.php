<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Services\MoneyConverter;

use Money\Currency;
use Money\Money;

/**
 * Interface ConverterInterface
 * @package Pizza\SharedKernel\Services\MoneyConverter
 */
interface ConverterInterface
{
    /**
     * @param Money $money
     * @param Currency $counterCurrency
     * @param int $roundingMode
     *
     * @return Money
     */
    public function convert(Money $money, Currency $counterCurrency, $roundingMode = Money::ROUND_HALF_UP): Money;
}
