<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Tests\Unit;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\User;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\MockObject\MockObject;
use Pizza\SharedKernel\Services\MoneyConverter\Accountant;
use Pizza\SharedKernel\Services\MoneyConverter\ConverterInterface;
use Pizza\SharedKernel\Tests\TestCase;

class AccountantTest extends TestCase
{
    /** @test */
    public function userChooseEUR()
    {
        /** @var ConverterInterface|MockObject $converter */
        $converter = $this->getMockBuilder(ConverterInterface::class)->getMock();
        $this->app->instance(ConverterInterface::class, $converter);
        $accountant = $this->app->make(Accountant::class);

        $money = $accountant->formatMoney(100);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals(100, $money->getAmount());
        $this->assertEquals('EUR', $money->getCurrency()->getCode());
    }

    /** @test */
    public function authUserChooseUSD()
    {
        /** @var User|MockObject $user */
        $user = $this->getMockBuilder(User::class)->getMock();
        $user->expects($this->once())
            ->method('getAttributeValue')
            ->willReturn('USD');

        /** @var Guard|MockObject $converter */
        $guard = $this->getMockBuilder(Guard::class)->getMock();
        $guard->expects($this->once())
            ->method('user')
            ->willReturn($user);
        $guard->expects($this->once())
            ->method('check')
            ->willReturn(true);

        /** @var ConverterInterface|MockObject $converter */
        $converter = $this->getMockBuilder(ConverterInterface::class)->getMock();
        $eur = new Money(100, new Currency('EUR'));
        $usd = new Money(118, new Currency('USD'));

        $converter->expects($this->once())->method('convert')
            ->with($eur, new Currency('USD'))
            ->willReturn($usd);

        $this->app->instance(ConverterInterface::class, $converter);
        $this->app->instance(Guard::class, $guard);
        $accountant = $this->app->make(Accountant::class);

        $money = $accountant->formatMoney(100);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals(118, $money->getAmount());
        $this->assertEquals('USD', $money->getCurrency()->getCode());
    }

    /** @test */
    public function guestChooseUSD()
    {
        /** @var Session|MockObject $converter */
        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->once())
            ->method('get')
            ->with('user.currency')
            ->willReturn('USD');

        /** @var ConverterInterface|MockObject $converter */
        $converter = $this->getMockBuilder(ConverterInterface::class)->getMock();
        $eur = new Money(100, new Currency('EUR'));
        $usd = new Money(118, new Currency('USD'));

        $converter->expects($this->once())->method('convert')
            ->with($eur, new Currency('USD'))
            ->willReturn($usd);

        $this->app->instance(ConverterInterface::class, $converter);
        $this->app->instance(Session::class, $session);
        $accountant = $this->app->make(Accountant::class);

        $money = $accountant->formatMoney(100);

        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals(118, $money->getAmount());
        $this->assertEquals('USD', $money->getCurrency()->getCode());
    }
}
