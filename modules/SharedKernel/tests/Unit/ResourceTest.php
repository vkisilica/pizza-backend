<?php

declare(strict_types=1);

namespace Pizza\SharedKernel\Tests\Unit;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Pizza\SharedKernel\Http\Resources\Resource;
use Pizza\SharedKernel\Tests\TestCase;

class ResourceTest extends TestCase
{
    /** @test */
    public function toIso8601Converting()
    {
        $mock = $this->getMockBuilder(Model::class)->getMock();
        $mock->method('toArray')
            ->willReturn(['at' => '2020-01-31 00:00']);
        $mock->method('getDates')
            ->willReturn(['at']);
        $mock->method('getAttributeValue')
            ->with('at')
            ->willReturn(Carbon::create(2020, 1, 31));

        $resource = new class($mock) extends Resource {};

        $expected = [
           'at' => '2020-01-31T00:00:00+00:00'
        ];
        $this->assertSame($expected, $resource->toArray(request()));
    }
}
