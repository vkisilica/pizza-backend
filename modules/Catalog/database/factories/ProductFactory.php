<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use Pizza\Catalog\Models\Product;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->name(),
        'price' => new \Money\Money(
            mt_rand(100, 1000),
            new \Money\Currency(config('money.default.currency'))
        ),
        'description' => $faker->paragraph(1),
    ];
});
