<?php

declare(strict_types=1);

namespace Pizza\Catalog\Seeders;

use Illuminate\Database\Seeder;
use Pizza\Catalog\Models\Product;

/**
 * Class CatalogSeeder
 * @package Pizza\Catalog\Seeders
 */
class CatalogSeeder extends Seeder
{
    public function run()
    {
        factory(Product::class, 20)->create();
    }
}
