<?php

declare(strict_types=1);

namespace Pizza\Catalog\Tests\Feature;

use Carbon\Carbon;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Catalog\Tests\TestCase;
use Pizza\SharedKernel\Services\MoneyConverter\AccountantInterface;

class ProductListTest extends TestCase
{
    /** @test */
    public function productList()
    {
        factory(Product::class, 20)->create();

        $response = $this->json('GET', 'api/catalog/products');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status', 'data']);
        $this->assertCount(20, $response->json('data'));
    }

    /** @test */
    public function productPresentation()
    {
        Carbon::setTestNow(Carbon::create(2020, 3, 28));
        $payload = [
            "id" => 1,
            "title" => "Zetta McClure",
            "price" => new Money(144, new Currency('EUR')),
            "description" => "Debitis vel ea minima et harum doloremque quae.",
        ];
        factory(Product::class)->create($payload);

        $accountant = $this->getMockBuilder(AccountantInterface::class)->getMock();
        $accountant->method('formatMoney')
            ->with(144)
            ->willReturn(new Money(144, new Currency('EUR')));

        $this->app->instance(AccountantInterface::class, $accountant);

        $response = $this->json('GET', 'api/catalog/products');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status', 'data']);
        $this->assertCount(1, $response->json('data'));

        $product = $response->json('data.0');

        $expected = array_merge($payload, [
            'price' => 1.44,
            'currency' => 'EUR',
            'created_at' => '2020-03-28T00:00:00+00:00',
            'updated_at' => '2020-03-28T00:00:00+00:00'
        ]);
        $this->assertEquals($expected, $product);
    }

    /** @test */
    public function emptyProductList()
    {
        $response = $this->json('GET', 'api/catalog/products');
        $response->assertStatus(200);
        $response->assertJsonStructure(['status', 'data']);
        $this->assertCount(0, $response->json('data'));
    }
}
