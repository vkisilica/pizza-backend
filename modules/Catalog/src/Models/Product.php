<?php

namespace Pizza\Catalog\Models;

use Illuminate\Database\Eloquent\Model;
use Pizza\SharedKernel\Traits\HasPriceAttribute;

/**
 * Class Product
 * @package Pizza\Catalog\Models
 */
class Product extends Model
{
    use HasPriceAttribute;

    const TABLE_NAME = 'catalog_products';

    const ID_COLUMN = 'id';
    const TITLE_COLUMN = 'title';
    const PRICE_COLUMN = 'price';
    const DESCRIPTION_COLUMN = 'description';

    protected $table = self::TABLE_NAME;

    protected $fillable = ['title', 'price', 'description'];
}
