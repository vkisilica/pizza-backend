<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace('Pizza\Catalog\Http\Controllers')
    ->group(function () {
    Route::prefix('catalog')->group(function () {
        Route::get('products', 'Products\IndexController');
    });
});
