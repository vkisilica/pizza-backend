<?php

declare(strict_types=1);

namespace Pizza\Catalog\Http\Controllers\Products;

use Illuminate\Http\Request;
use Pizza\Catalog\Http\Controllers\Controller as BaseController;
use Pizza\Catalog\Models\Product;

class IndexController extends BaseController
{
    public function __invoke(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'data' => ProductResourceCollection::make(Product::query()->get())
        ]);
    }
}
