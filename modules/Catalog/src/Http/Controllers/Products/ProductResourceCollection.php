<?php

declare(strict_types=1);

namespace Pizza\Catalog\Http\Controllers\Products;

use Pizza\Catalog\Http\Controllers\ResourceCollection;

class ProductResourceCollection extends ResourceCollection
{
    public $collects = ProductResource::class;
}
