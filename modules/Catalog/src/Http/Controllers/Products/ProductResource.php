<?php

declare(strict_types=1);

namespace Pizza\Catalog\Http\Controllers\Products;

use Money\MoneyFormatter;
use Pizza\Catalog\Http\Controllers\Resource;
use Pizza\Catalog\Models\Product;

/**
 * Class ProductResource
 * @package Pizza\Catalog\Http\Controllers\Products
 */
class ProductResource extends Resource
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /** @inheritDoc */
    public function __construct($resource)
    {
        $this->moneyFormatter = app(MoneyFormatter::class);

        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request)
    {
        $array = parent::toArray($request);

        if ($this->resource instanceof Product) {
            $money = $this->resource->getPriceAttribute();
            $array['price'] = $this->moneyFormatter->format($money);
            $array['currency'] = $money->getCurrency()->getCode();
        }

        return $array;
    }
}
