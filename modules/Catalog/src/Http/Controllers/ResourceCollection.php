<?php

declare(strict_types=1);

namespace Pizza\Catalog\Http\Controllers;

abstract class ResourceCollection extends \Pizza\SharedKernel\Http\Resources\ResourceCollection
{

}
