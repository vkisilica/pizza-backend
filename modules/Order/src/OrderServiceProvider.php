<?php

declare(strict_types=1);

namespace Pizza\Order;

use Illuminate\Support\ServiceProvider;
use Pizza\Order\Repositories\Cart\CartRepositoryInterface;
use Pizza\Order\Repositories\Cart\SessionCartRepository;
use Pizza\SharedKernel\SharedKernelServiceProvider;

class OrderServiceProvider extends ServiceProvider
{
    /** @inheritDoc */
    public function boot()
    {
        $this->loadFactoriesFrom(__DIR__ . '/../database/factories');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');

        $this->publishes([
            realpath(__DIR__.'/../config/order.php') => config_path('order.php'),
        ], 'config');
    }

    /** @inheritDoc */
    public function register()
    {
        parent::register();

        $this->app->register(SharedKernelServiceProvider::class);

        $this->mergeConfigFrom(
            realpath(__DIR__.'/../config/order.php'), 'order'
        );

        $this->app->singleton(CartRepositoryInterface::class, SessionCartRepository::class);
    }
}
