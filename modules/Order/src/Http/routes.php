<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace('Pizza\Order\Http\Controllers')
    ->group(function () {
    Route::prefix('orders')->group(function () {
        Route::post('/', 'Orders\StoreController');
        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('/history', 'Orders\HistoryController');
        });

        Route::prefix('addresses')->group(function() {
            Route::get('streets', 'Orders\Addresses\IndexStreetController');
        });

        Route::prefix('cart')->group(function() {
            Route::get('/', 'Orders\Cart\ShowController');
            Route::prefix('products')->group(function() {
                Route::put('{productId}', 'Orders\Cart\Products\StoreProductController');
                Route::delete('{productId}', 'Orders\Cart\Products\DeleteProductController');
            });
        });
    });
});
