<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Money\MoneyFormatter;
use Pizza\Order\Http\Controllers\Resource;
use Pizza\Order\Models\Order;

class OrderResource extends Resource
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /** @inheritDoc */
    public function __construct($resource)
    {
        $this->moneyFormatter = app(MoneyFormatter::class);

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        /** @var Order $resource */
        $order = $this->resource;
        $output = [
            'contact' => ContactResource::make($this->whenLoaded(Order::CONTACT_RELATION)),
            'products' => OrderProductResourceCollection::make($this->whenLoaded(Order::PRODUCTS_RELATION)),
            'cart_cost' => (float)$this->moneyFormatter->format($order->getCartCost()),
            'delivery_cost' => (float)$this->moneyFormatter->format($order->getDeliveryCost()),
            'total_cost' => (float)$this->moneyFormatter->format($order->getTotalCost()),
        ];

        return array_merge(parent::toArray($request), $output);
    }
}
