<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Pizza\Order\Http\Controllers\ResourceCollection;

class OrderProductResourceCollection extends ResourceCollection
{
    public $collects = OrderProductResource::class;
}
