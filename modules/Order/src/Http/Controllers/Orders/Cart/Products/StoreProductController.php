<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Cart\Products;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Http\Controllers\Controller;
use Pizza\Order\Http\Controllers\Orders\Cart\CartResource;
use Pizza\Order\Repositories\Cart\CartRepositoryInterface;

class StoreProductController extends Controller
{
    /**
     * @param $productId
     * @param Request $request
     * @param CartRepositoryInterface $cartRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke($productId, Request $request, CartRepositoryInterface $cartRepository)
    {
//        dd($request->headers);
        $request->merge(['product_id' => $productId]);
        $v = Validator::make($request->all(), [
            'product_id' => sprintf('required|exists:%s,%s', Product::class, Product::ID_COLUMN),
            'amount' => 'required|integer'
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $cart = $cartRepository->query();

        $product = Product::find($request->get('product_id'));
        $cart->addProduct($product, (int)$request->get('amount'));

        $cartRepository->save($cart);

        return response()->json([
            'status' => 'success',
            'data' => CartResource::make($cart)
        ]);
    }
}
