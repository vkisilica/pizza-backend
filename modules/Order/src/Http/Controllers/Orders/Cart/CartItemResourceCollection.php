<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Cart;

use Pizza\Order\Http\Controllers\ResourceCollection;

class CartItemResourceCollection extends ResourceCollection
{
    public $collects = CartItemResource::class;
}
