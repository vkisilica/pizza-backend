<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Cart;

use Money\MoneyFormatter;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\Cart;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Http\Controllers\Resource;

/**
 * @property Cart $resource
 */
class CartResource extends Resource
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /** @inheritDoc */
    public function __construct($resource)
    {
        $this->moneyFormatter = app(MoneyFormatter::class);

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'products' => CartItemResourceCollection::make($this->resource->getProducts()),
            'cart_cost' => (float)$this->moneyFormatter->format($this->resource->getCartCost()),
            'delivery_cost' => (float)$this->moneyFormatter->format($this->resource->getDeliveryCost()),
            'total_cost' => (float)$this->moneyFormatter->format($this->resource->getTotalCost()),
        ];
    }
}
