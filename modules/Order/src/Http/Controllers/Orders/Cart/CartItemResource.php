<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Cart;

use Money\MoneyFormatter;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Http\Controllers\Resource;

/**
 * @property CartItem $resource
 */
class CartItemResource extends Resource
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /** @inheritDoc */
    public function __construct($resource)
    {
        $this->moneyFormatter = app(MoneyFormatter::class);

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $product = $this->resource->getProduct();
        return [
            'id' => $product->getKey(),
            'title' => $product->getAttributeValue(Product::TITLE_COLUMN),
            'price' => (float)$this->moneyFormatter->format($product->getAttributeValue(Product::PRICE_COLUMN)),
            'amount' => $this->resource->getAmount(),
            'cost' => (float)$this->moneyFormatter->format($this->resource->getCost()),
        ];
    }
}
