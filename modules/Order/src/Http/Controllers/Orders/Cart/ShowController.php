<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Cart;

use Illuminate\Http\Request;
use Pizza\Order\Http\Controllers\Controller;
use Pizza\Order\Repositories\Cart\CartRepositoryInterface;

class ShowController extends Controller
{
    /**
     * @param Request $request
     * @param CartRepositoryInterface $cartRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, CartRepositoryInterface $cartRepository)
    {
        $cart = $cartRepository->query();

        return response()->json([
            'status' => 'success',
            'data' => CartResource::make($cart)
        ]);
    }
}
