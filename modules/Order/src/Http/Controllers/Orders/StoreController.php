<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Money\MoneyFormatter;
use Pizza\Account\Models\User;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Http\Controllers\Controller as BaseController;
use Pizza\Order\Models\Address\Address;
use Pizza\Order\Models\Address\Street;
use Pizza\Order\Models\Contact;
use Pizza\Order\Models\Order;
use Pizza\Order\Models\OrderProduct;
use Pizza\Order\Repositories\Cart\CartRepositoryInterface;

class StoreController extends BaseController
{
    public function __invoke(
        Request $request,
        Guard $guard,
        Session $session,
        Repository $config,
        CartRepositoryInterface $cartRepository,
        MoneyFormatter $moneyFormatter
    )
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required|regex:/^\(0[0-9]{4,4}\)\s[0-9]{6,6}$/i',
            'street_id' => sprintf('required|exists:%s,%s', Street::class, Street::ID_COLUMN),
            'house' => 'required|string|max:10',
            'apartment' => 'nullable|string|max:255'
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $order = null;
        DB::transaction(function () use (&$order, $request, $guard, $cartRepository, $session, $config, $moneyFormatter) {
            $userId = null;
            if ($guard->check()) {
                /** @var User $user */
                $user = $guard->user();
                $userId = $user->getKey();
                $currency = $user->getAttributeValue(User::CURRENCY_COLUMN);
            } else {
                $currency = $session->get('user.currency', $config->get('money.default.currency'));
            }

            $cart = $cartRepository->query();

            $contact = Contact::firstOrCreate($request->only(['email', 'phone']));
            $address = Address::firstOrCreate($request->only(['street_id', 'house', 'apartment']));

            $order = Order::create([
                'user_id' => $userId,
                'contact_id' => $contact->getKey(),
                'address_id' => $address->getKey(),
                'currency' => $currency,
                'delivery_cost' => $cart->getDeliveryCost(),
            ]);

            /** @var CartItem $cartItem */
            foreach ($cart->getProducts() as $cartItem) {
                $product = $cartItem->getProduct();
                OrderProduct::create([
                    'order_id' => $order->getKey(),
                    'product_id' => $product->getKey(),
                    'price' => $product->getAttributeValue(Product::PRICE_COLUMN),
                    'amount' => $cartItem->getAmount(),
                ]);
            }
            $cart->clear();
            $cart = $cartRepository->save($cart);

        }, 3);

        return response()->json([
            'status' => 'success',
            'data' => OrderResource::make($order->fresh([
                Order::PRODUCTS_RELATION,
                Order::CONTACT_RELATION,
                Order::PRODUCTS_RELATION . '.' . OrderProduct::ORDER_RELATION,
                Order::PRODUCTS_RELATION . '.' . OrderProduct::PRODUCT_RELATION,
                Order::ADDRESS_RELATION . '.' . Address::STREET_RELATION
            ]))
        ])->setStatusCode(201);
    }
}
