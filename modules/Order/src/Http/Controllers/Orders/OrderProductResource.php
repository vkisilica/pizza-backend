<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Money\MoneyFormatter;
use Pizza\Catalog\Http\Controllers\Products\ProductResource;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Http\Controllers\Resource;
use Pizza\Order\Models\Order;
use Pizza\Order\Models\OrderProduct;

/**
 * @property OrderProduct $resource
 */
class OrderProductResource extends Resource
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /** @inheritDoc */
    public function __construct($resource)
    {
        $this->moneyFormatter = app(MoneyFormatter::class);

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $output = [
            'product' => ProductResource::make($this->whenLoaded(OrderProduct::PRODUCT_RELATION)),
            'price' => (float)$this->moneyFormatter->format($this->resource->getAttributeValue(OrderProduct::PRICE_COLUMN)),
            'cost' => (float)$this->moneyFormatter->format($this->resource->getCost()),
        ];

        return array_merge(parent::toArray($request), $output);
    }
}
