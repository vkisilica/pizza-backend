<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Pizza\Order\Http\Controllers\Controller as BaseController;
use Pizza\Order\Models\Address\Address;
use Pizza\Order\Models\Order;
use Pizza\Order\Models\OrderProduct;

class HistoryController extends BaseController
{
    public function __invoke(
        Request $request,
        Guard $guard
    ){
        $orders = Order::query()
            ->with([
                Order::USER_RELATION,
                Order::CONTACT_RELATION,
                Order::PRODUCTS_RELATION . '.' . OrderProduct::PRODUCT_RELATION,
                Order::PRODUCTS_RELATION . '.' . OrderProduct::ORDER_RELATION,
                Order::ADDRESS_RELATION . '.' . Address::STREET_RELATION,
            ])
            ->where(Order::USER_ID_COLUMN, $guard->id())
            ->orderBy(Order::CREATED_AT, 'desc')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => OrderResourceCollection::make($orders)
        ]);
    }
}
