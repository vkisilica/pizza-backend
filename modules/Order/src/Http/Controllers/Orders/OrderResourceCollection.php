<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders;

use Pizza\Order\Http\Controllers\ResourceCollection;

class OrderResourceCollection extends ResourceCollection
{
    public $collects = OrderResource::class;
}
