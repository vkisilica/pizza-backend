<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Addresses;

use Illuminate\Http\Request;
use Pizza\Order\Http\Controllers\Controller;
use Pizza\Order\Models\Address\Street;

class IndexStreetController extends Controller
{
    public function __invoke(Request $request)
    {
        $streets = Street::query()
            ->orderBy(Street::TITLE_COLUMN, 'asc')
            ->get();

        return response()->json([
            'status' => 'success',
            'data' => StreetResourceCollection::make($streets)
        ]);
    }
}
