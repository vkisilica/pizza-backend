<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers\Orders\Addresses;

use Pizza\Order\Http\Controllers\ResourceCollection;

class StreetResourceCollection extends ResourceCollection
{
    public $collects = StreetResource::class;
}
