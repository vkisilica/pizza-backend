<?php

declare(strict_types=1);

namespace Pizza\Order\Http\Controllers;

abstract class ResourceCollection extends \Pizza\SharedKernel\Http\Resources\ResourceCollection
{

}
