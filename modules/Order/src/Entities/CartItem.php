<?php

declare(strict_types=1);

namespace Pizza\Order\Entities;

use Money\Money;
use Pizza\Catalog\Models\Product;

/**
 * Class CartItem
 * @package Pizza\Order\Entities
 */
class CartItem
{
    private $product;
    private $amount;

    /**
     * CartItem constructor.
     * @param Product $product
     * @param int $amount
     */
    public function __construct(Product $product, int $amount)
    {
        $this->product = $product;
        $this->amount = $amount;
    }

    /**
     * @return Money
     */
    public function getCost(): Money
    {
        /** @var Money $price */
        $price = $this->getProduct()->getAttributeValue(Product::PRICE_COLUMN);
        return $price->multiply($this->getAmount());
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
