<?php

declare(strict_types=1);

namespace Pizza\Order\Entities;

use Illuminate\Support\Collection;
use Money\Money;
use Pizza\Catalog\Models\Product;

class Cart
{
    /** @var Collection  */
    private $products;
    /** @var Money  */
    private $deliveryCost;

    /**
     * Cart constructor.
     * @param Collection $products
     * @param Money $deliveryCost
     */
    public function __construct(Collection $products, Money $deliveryCost)
    {
        $this->products = $products;
        $this->deliveryCost = $deliveryCost;
    }

    /**
     * @param Product $product
     * @param int $amount
     * @return void
     */
    public function addProduct(Product $product, int $amount)
    {
        if ($this->products->offsetExists('product-' . $product->getKey())) {
            $cartItem = $this->products->offsetGet('product-' . $product->getKey());
            $amount += $cartItem->getAmount();
        }
        $cartItem = new CartItem($product, $amount);
        $this->products->offsetSet('product-' . $product->getKey(), $cartItem);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function removeProduct(Product $product)
    {
        $this->products->offsetUnset('product-' . $product->getKey());
    }

    /**
     * @return void
     */
    public function clear(): void
    {
        $this->products = collect();
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return Money
     */
    public function getCartCost(): Money
    {
        $cost = new Money(0, $this->deliveryCost->getCurrency());
        /** @var CartItem $cartItem */
        foreach ($this->products as $cartItem) {
            /** @var Product $product */
            $productCost = $cartItem->getCost();
            $cost = $cost->add($productCost);
        }
        return $cost;
    }

    /**
     * @return Money
     */
    public function getDeliveryCost(): Money
    {
        return $this->deliveryCost;
    }

    /**
     * @return Money
     */
    public function getTotalCost(): Money
    {
        return $this->getCartCost()->add($this->deliveryCost);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->products->count() === 0;
    }
}
