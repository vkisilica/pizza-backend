<?php

declare(strict_types=1);

namespace Pizza\Order\Repositories\Cart;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Money\MoneyFormatter;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\Cart;
use Pizza\Order\Entities\CartItem;
use Pizza\SharedKernel\Services\MoneyConverter\AccountantInterface;

/**
 * Class SessionCartRepository
 * @package Pizza\Order\Repositories\Cart
 */
class SessionCartRepository implements CartRepositoryInterface
{
    /** @var Session  */
    protected $session;
    /** @var AccountantInterface  */
    protected $accountant;
    /** @var Repository  */
    protected $config;

    /**
     * SessionCartRepository constructor.
     * @param Session $session
     * @param AccountantInterface $accountant
     * @param Repository $config
     */
    public function __construct(Session $session, AccountantInterface $accountant, Repository $config)
    {
        $this->session = $session;
        $this->accountant = $accountant;
        $this->config = $config;
    }

    /**
     * @return Cart
     */
    public function query(): Cart
    {
        $cart = $this->getCartFromSession();

        $deliveryCost = $this->accountant->formatMoney((int)$cart['delivery_cost']);
        $products = $this->prepareProductsCollection($cart);
        return new Cart($products, $deliveryCost);
    }

    /**
     * @param Cart $cart
     * @return void
     */
    public function save(Cart $cart): void
    {
        $cartArray = [
            'products' => $this->makeCartItemArrayFromCollection($cart->getProducts()),
            'delivery_cost' => $cart->getDeliveryCost()->getAmount()
        ];
        $this->session->put('cart', $cartArray);
//        $this->session->save();
    }

    /**
     * @return array
     */
    private function getCartFromSession(): array
    {
        $defaultCart = [
            'products' => [],
            'delivery_cost' => $this->config->get('order.delivery_cost')
        ];
        return $this->session->get('cart', $defaultCart);
    }

    /**
     * @param array $cart
     * @return Collection
     */
    private function prepareProductsCollection(array $cart): Collection
    {
        if (count($cart['products']) === 0) {
            return collect();
        }

        return $this->makeCartItemCollectionFromArray(Arr::get($cart, 'products'));
    }

    /**
     * @param array $cartProducts
     * @return Collection
     */
    private function makeCartItemCollectionFromArray(array $cartProducts): Collection
    {
        $productIds = Arr::pluck($cartProducts, 'id');
        $products = Product::query()->whereIn(Product::ID_COLUMN, $productIds)->get();

        $cartItems = collect();
        foreach ($cartProducts as $productArray) {
            $product = $products->where(Product::ID_COLUMN, $productArray['id'])->first();
            if ($product === null) {
                throw (new ModelNotFoundException())->setModel(Product::class, $productArray['id']);
            }

            $cartItems->put('product-' . $productArray['id'], new CartItem($product, $productArray['amount']));
        }

        return $cartItems;
    }

    /**
     * @param Collection $cartItemCollection
     * @return array
     */
    private function makeCartItemArrayFromCollection(Collection $cartItemCollection): array
    {
        $cartItems = [];
        /** @var CartItem $cartItem */
        foreach ($cartItemCollection as $cartItem) {
            $product = $cartItem->getProduct();
            $cartItems['product-' . $product->getKey()] = [
                'id' => $product->getKey(),
                'amount' => $cartItem->getAmount()
            ];
        }

        return $cartItems;
    }
}
