<?php

declare(strict_types=1);

namespace Pizza\Order\Repositories\Cart;

use Pizza\Order\Entities\Cart;

interface CartRepositoryInterface
{
    public function query(): Cart;
    public function save(Cart $cart): void;
}
