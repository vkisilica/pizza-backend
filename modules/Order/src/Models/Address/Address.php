<?php

declare(strict_types=1);

namespace Pizza\Order\Models\Address;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    const TABLE_NAME = 'order_addresses';

    const STREET_ID_COLUMN = 'street_id';
    const HOUSE_COLUMN = 'house';
    const APARTMENT_COLUMN = 'apartment';

    const STREET_RELATION = 'street';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::STREET_ID_COLUMN,
        self::HOUSE_COLUMN,
        self::APARTMENT_COLUMN
    ];

    // Relations
    public function street()
    {
        return $this->belongsTo(Street::class);
    }
}
