<?php

declare(strict_types=1);

namespace Pizza\Order\Models\Address;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    const TABLE_NAME = 'order_streets';

    protected $table = self::TABLE_NAME;

    const ID_COLUMN = 'id';
    const TITLE_COLUMN = 'title';

    protected $fillable = [
        self::TITLE_COLUMN
    ];
}
