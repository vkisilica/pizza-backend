<?php

declare(strict_types=1);

namespace Pizza\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\SharedKernel\Services\MoneyConverter\AccountantInterface;
use Pizza\SharedKernel\Traits\HasPriceAttribute;
use RuntimeException;

class OrderProduct extends Model
{
    const TABLE_NAME = 'order_products';

    const ID_COLUMN = 'id';
    const ORDER_ID_COLUMN = 'order_id';
    const PRODUCT_ID_COLUMN = 'product_id';
    const PRICE_COLUMN = 'price';
    const AMOUNT_COLUMN = 'amount';

    const ORDER_RELATION = 'order';
    const PRODUCT_RELATION = 'product';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::ID_COLUMN,
        self::ORDER_ID_COLUMN,
        self::PRODUCT_ID_COLUMN,
        self::PRICE_COLUMN,
        self::AMOUNT_COLUMN,
    ];

    // Relations
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return Money
     */
    public function getCost(): Money
    {
        if (!$this->relationLoaded(static::PRODUCT_RELATION)) {
            throw new RuntimeException(sprintf('%s relations has not been loaded', static::PRODUCT_RELATION));
        }
        /** @var Money $price */
        $price = $this->getRelation(static::PRODUCT_RELATION)->getAttributeValue(Product::PRICE_COLUMN);
        return $price->multiply($this->getAttributeValue(static::AMOUNT_COLUMN));
    }

    /**
     * @return Money
     */
    public function getPriceAttribute(): Money
    {
        if (!$this->relationLoaded(static::ORDER_RELATION)) {
            throw new RuntimeException(sprintf('%s relations has not been loaded', static::ORDER_RELATION));
        }
        /** @var Order $order */
        $order = $this->getRelation(static::ORDER_RELATION);
        $currency = $order->getAttributevalue(Order::CURRENCY_COLUMN);
        return new Money((int)$this->attributes[static::PRICE_COLUMN], new Currency($currency));
    }

    /**
     * @param Money $value
     */
    public function setPriceAttribute(Money $value)
    {
        $this->attributes[static::PRICE_COLUMN] = $value->getAmount();
    }
}
