<?php

declare(strict_types=1);

namespace Pizza\Order\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const TABLE_NAME = 'order_contacts';

    const EMAIL_COLUMN = 'email';
    const PHONE_COLUMN = 'phone';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::EMAIL_COLUMN,
        self::PHONE_COLUMN
    ];
}
