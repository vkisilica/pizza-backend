<?php

declare(strict_types=1);

namespace Pizza\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Money\Currency;
use Money\Money;
use Pizza\Account\Models\User;
use Pizza\Order\Models\Address\Address;

class Order extends Model
{
    const TABLE_NAME = 'orders';

    const USER_ID_COLUMN = 'user_id';
    const CONTACT_ID_COLUMN = 'contact_id';
    const ADDRESS_ID_COLUMN = 'address_id';
    const CURRENCY_COLUMN = 'currency';
    const CART_COST_COLUMN = 'cart_cost';
    const DELIVERY_COST_COLUMN = 'delivery_cost';
    const TOTAL_COST_COLUMN = 'total_cost';

    const USER_RELATION = 'user';
    const CONTACT_RELATION = 'contact';
    const ADDRESS_RELATION = 'address';
    const PRODUCTS_RELATION = 'products';

    protected $table = self::TABLE_NAME;

    protected $fillable = [
        self::USER_ID_COLUMN,
        self::CONTACT_ID_COLUMN,
        self::ADDRESS_ID_COLUMN,
        self::CURRENCY_COLUMN,
        self::CART_COST_COLUMN,
        self::DELIVERY_COST_COLUMN,
        self::TOTAL_COST_COLUMN,
    ];

    // Relations

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    /**
     * @return Money
     */
    public function getDeliveryCostAttribute(): Money
    {
        return new Money((int)$this->attributes[static::DELIVERY_COST_COLUMN], new Currency($this->getAttributeValue(static::CURRENCY_COLUMN)));
    }

    /**
     * @param Money $value
     */
    public function setDeliveryCostAttribute(Money $value)
    {
        $this->attributes[static::DELIVERY_COST_COLUMN] = $value->getAmount();
    }

    /**
     * @return Money
     */
    public function getCartCost(): Money
    {
        if (!$this->relationLoaded(static::PRODUCTS_RELATION)) {
            throw new \RuntimeException(sprintf('%s relation has not been loaded', static::PRODUCTS_RELATION));
        }

        $sum = new Money(0, new Currency($this->getAttributeValue(static::CURRENCY_COLUMN)));
        /** @var OrderProduct $orderProduct */
        foreach ($this->getRelation(static::PRODUCTS_RELATION) as $orderProduct) {
            /** @var Money $productPrice */
            $productPrice = $orderProduct->getAttributeValue(OrderProduct::PRICE_COLUMN);
            $productCost = $productPrice->multiply($orderProduct->getAttributeValue(OrderProduct::AMOUNT_COLUMN));
            $sum = $sum->add($productCost);
        }

        return $sum;
    }

    /**
     * @return Money
     */
    public function getDeliveryCost(): Money
    {
        return $this->getAttributeValue(static::DELIVERY_COST_COLUMN);
    }

    /**
     * @return Money
     */
    public function getTotalCost(): Money
    {
        return $this->getCartCost()->add($this->getDeliveryCost());
    }
}
