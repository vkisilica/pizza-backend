<?php

declare(strict_types=1);

namespace Pizza\Order\Seeders;

use Illuminate\Database\Seeder;
use Pizza\Order\Models\Address\Street;

/**
 * Class OrderSeeder
 * @package Pizza\Order\Seeders
 */
class OrderSeeder extends Seeder
{
    public function run()
    {
        factory(Street::class, 20)->create();
    }
}
