<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('contact_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->string('currency', 3);
            $table->integer('delivery_cost')->unsigned();
            $table->timestamps();

            $table->index('user_id');

            $table->foreign('user_id')->references('id')->on('account_users');
            $table->foreign('contact_id')->references('id')->on('order_contacts');
            $table->foreign('address_id')->references('id')->on('order_addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
