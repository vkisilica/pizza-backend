<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Pizza\Order\Models\Address\Address::class, function (Faker $faker) {
    return [
        'street_id' => function() {
            return factory(\Pizza\Order\Models\Address\Street::class)->create()->getKey();
        },
        'house' => $faker->buildingNumber,
        'apartment' => $faker->name,
    ];
});
