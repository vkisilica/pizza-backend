<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Pizza\Order\Models\Order::class, function (Faker $faker) {
    $currencies = ['EUR', 'USD'];
    $currency = $currencies[mt_rand(0, 1)];

    return [
        'user_id' => function() {
            return factory(\Pizza\Account\Models\User::class)->create()->getKey();
        },
        'contact_id' => function() {
            return factory(\Pizza\Order\Models\Contact::class)->create()->getKey();
        },
        'address_id' => function() {
            return factory(\Pizza\Order\Models\Address\Address::class)->create()->getKey();
        },
        'currency' => $currency,
        'delivery_cost' => new Money\Money(mt_rand(1, 1000), new \Money\Currency($currency))
    ];
});
