<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Pizza\Order\Models\OrderProduct::class, function (Faker $faker) {
    $currency = new \Money\Currency('EUR');
    $price = new Money\Money(100, $currency);

    return [
        'order_id' => function() use ($currency) {
            return factory(\Pizza\Order\Models\Order::class)->create([
                'currency' => $currency
            ])->getKey();
        },
        'product_id' => function() {
            return factory(\Pizza\Catalog\Models\Product::class)->create()->getKey();
        },
        'price' => $price,
        'amount' => mt_rand(1, 10),
    ];
});
