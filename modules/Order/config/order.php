<?php

declare(strict_types=1);

return [
    'delivery_cost' => (int)env('ORDER_DELIVERY_COST', 0)
];
