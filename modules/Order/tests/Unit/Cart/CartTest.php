<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Unit\Cart;

use Illuminate\Support\Collection;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\Cart;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Tests\TestCase;

class CartTest extends TestCase
{
    /** @test */
    public function basic()
    {
        $products = factory(Product::class, 3)->create([
            'price' => new Money(100, new Currency('EUR'))
        ]);

        $cartItems = collect();
        $cartItems->put($products[0]->getKey(), new CartItem($products[0], 1));
        $cartItems->put($products[1]->getKey(), new CartItem($products[1], 1));
        $cartItems->put($products[2]->getKey(), new CartItem($products[2], 3));

        $cart = new Cart($cartItems, new Money(200, new Currency('EUR')));

        $this->assertEquals(new Money(500, new Currency('EUR')), $cart->getCartCost());
        $this->assertEquals(new Money(200, new Currency('EUR')), $cart->getDeliveryCost());
        $this->assertEquals(new Money(700, new Currency('EUR')), $cart->getTotalCost());
        $this->assertInstanceOf(Collection::class, $cart->getProducts());
        $this->assertEquals(3, $cart->getProducts()->count());
        $this->assertInstanceOf(CartItem::class, $cart->getProducts()->first());
    }

    /** @test */
    public function addProduct()
    {
        $product = factory(Product::class)->create();

        $cart = new Cart(collect(), new Money(200, new Currency('EUR')));
        $this->assertEquals(0, $cart->getProducts()->count());
        $cart->addProduct($product, 3);

        $this->assertEquals(1, $cart->getProducts()->count());
        $this->assertEquals(1, $cart->getProducts()->first()->getProduct()->getKey());
        $this->assertEquals(3, $cart->getProducts()->first()->getAmount());

        $cart->addProduct($product, 1);

        $this->assertEquals(1, $cart->getProducts()->count());
        $this->assertEquals(1, $cart->getProducts()->first()->getProduct()->getKey());
        $this->assertEquals(4, $cart->getProducts()->first()->getAmount());
    }

    /** @test */
    public function removeProduct()
    {
        $product = factory(Product::class)->create();

        $cart = new Cart(collect(), new Money(200, new Currency('EUR')));
        $this->assertEquals(0, $cart->getProducts()->count());
        $cart->addProduct($product, 3);

        $this->assertEquals(1, $cart->getProducts()->count());
        $this->assertEquals(1, $cart->getProducts()->first()->getProduct()->getKey());
        $this->assertEquals(3, $cart->getProducts()->first()->getAmount());

        $cart->removeProduct($product);

        $this->assertTrue($cart->isEmpty());
    }
}
