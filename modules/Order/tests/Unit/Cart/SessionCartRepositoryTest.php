<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Unit\Cart;

use Illuminate\Contracts\Session\Session;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Entities\Cart;
use Pizza\Order\Entities\CartItem;
use Pizza\Order\Repositories\Cart\CartRepositoryInterface;
use Pizza\Order\Tests\TestCase;

class SessionCartRepositoryTest extends TestCase
{
    /** @test */
    public function query()
    {
        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->at(0))
            ->method('get')
            ->with('cart')
            ->willReturn([
                'products' => [],
                'delivery_cost' => 199
            ]);
        $session->expects($this->at(1))
            ->method('get')
            ->with('user.currency')
            ->willReturn('EUR');

        $this->app->instance(Session::class, $session);
        $repository = $this->app->make(CartRepositoryInterface::class);
        $cart = $repository->query();

        $this->assertInstanceOf(Cart::class, $cart);
        $this->assertTrue($cart->isEmpty());
        $this->assertEquals(new Money(199, new Currency('EUR')), $cart->getDeliveryCost());
        $this->assertEquals(new Money(0, new Currency('EUR')), $cart->getCartCost());
        $this->assertEquals(new Money(199, new Currency('EUR')), $cart->getTotalCost());
    }

    /** @test */
    public function save()
    {
        $products = factory(Product::class, 3)->create([
            'price' => new Money(100, new Currency('EUR'))
        ]);

        $cartItems = collect();
        $cartItems->put($products[0]->getKey(), new CartItem($products[0], 1));
        $cartItems->put($products[1]->getKey(), new CartItem($products[1], 1));
        $cartItems->put($products[2]->getKey(), new CartItem($products[2], 3));

        $cart = new Cart($cartItems, new Money(200, new Currency('EUR')));

        $cartArray = [
            'products' => [
                'product-1' => [
                    'id' => 1,
                    'amount' => 1
                ],
                'product-2' => [
                    'id' => 2,
                    'amount' => 1
                ],
                'product-3' => [
                    'id' => 3,
                    'amount' => 3
                ]
            ],
            'delivery_cost' => 200
        ];
        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->at(0))
            ->method('put')
            ->with('cart', $cartArray);

        $this->app->instance(Session::class, $session);
        $repository = $this->app->make(CartRepositoryInterface::class);

        $repository->save($cart);
    }
}
