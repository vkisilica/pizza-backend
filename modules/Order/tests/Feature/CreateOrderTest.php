<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Feature;

use Illuminate\Contracts\Session\Session;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Models\Address\Street;
use Pizza\Order\Tests\TestCase;

class CreateOrderTest extends TestCase
{
    /** @test */
    public function create()
    {
        $this->withoutExceptionHandling();

        $street = factory(Street::class)->create();
        $products = factory(Product::class, 3)->create([
            'price' => new Money(100, new Currency('EUR'))
        ]);

        $cart = [
            'products' => [
                $products[0]->getKey() => [
                    'id' => $products[0]->getKey(),
                    'amount' => 1
                ],
                $products[1]->getKey() => [
                    'id' => $products[1]->getKey(),
                    'amount' => 1
                ],
                $products[2]->getKey() => [
                    'id' => $products[2]->getKey(),
                    'amount' => 3
                ]
            ],
            'delivery_cost' => 299,
        ];
        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->at(0))
            ->method('get')
            ->with('user.currency')
            ->willReturn('EUR');
        $session->expects($this->at(1))
            ->method('get')
            ->with('cart')
            ->willReturn($cart);

        $this->app->instance(Session::class, $session);

        $this->assertDatabaseMissing('order_products', ['order_id' => 1]);

        $response = $this->json('POST', 'api/orders', [
            'email' => 'customer@test.com',
            'phone' => '(01111) 222222',
            'street_id' => $street->getKey(),
            'house' => '7',
            'apartment' => '28'
        ]);
        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'cart_cost' => '5.00',
                'delivery_cost' => '2.99',
                'total_cost' => '7.99',
                'currency' => 'EUR'
            ]
        ]);

        $this->assertDatabaseHas('order_products', ['order_id' => 1]);
    }
}
