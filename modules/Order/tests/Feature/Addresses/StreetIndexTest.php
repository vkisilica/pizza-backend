<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Feature\Addresses;

use Pizza\Order\Models\Address\Street;
use Pizza\Order\Tests\TestCase;

class StreetIndexTest extends TestCase
{
    /** @test */
    public function index()
    {
        $street1 = factory(Street::class)->create([
            Street::TITLE_COLUMN => 'a'
        ]);
        $street2 = factory(Street::class)->create([
            Street::TITLE_COLUMN => 'c'
        ]);
        $street3 = factory(Street::class)->create([
            Street::TITLE_COLUMN => 'b'
        ]);

        $response = $this->json('GET', 'api/orders/addresses/streets');
        $response->assertStatus(200);
        $response->assertJsonCount(3, 'data');
        $response->assertJson([
            'data' => [
                [
                    'id' => $street1->getKey(),
                    'title' => $street1->getAttributeValue(Street::TITLE_COLUMN),
                ],
                [
                    'id' => $street3->getKey(),
                    'title' => $street3->getAttributeValue(Street::TITLE_COLUMN),
                ],
                [
                    'id' => $street2->getKey(),
                    'title' => $street2->getAttributeValue(Street::TITLE_COLUMN),
                ]
            ]
        ]);
    }
}
