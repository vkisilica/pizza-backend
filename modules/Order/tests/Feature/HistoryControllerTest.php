<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Feature;

use Money\MoneyFormatter;
use Pizza\Account\Models\User;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Models\Address\Address;
use Pizza\Order\Models\Address\Street;
use Pizza\Order\Models\Contact;
use Pizza\Order\Models\Order;
use Pizza\Order\Models\OrderProduct;
use Pizza\Order\Tests\TestCase;

class HistoryControllerTest extends TestCase
{
    /** @test */
    public function guest()
    {
        $response = $this->json('GET', 'api/orders/history');
        $response->assertStatus(200);
        $response->assertJson(['error' => "Unauthorized"]);
    }

    /** @test */
    public function authenticatedUser()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $this->be($user);
        $orders = factory(Order::class, 3)->create([
            'user_id' => $user->getKey()
        ]);

        factory(OrderProduct::class, 3)->create([
            'order_id' => $orders[0]->getKey()
        ]);
        $orders = $orders->fresh([
            Order::USER_RELATION,
            Order::CONTACT_RELATION,
            Order::PRODUCTS_RELATION . '.' . OrderProduct::PRODUCT_RELATION,
            Order::PRODUCTS_RELATION . '.' . OrderProduct::ORDER_RELATION,
            Order::ADDRESS_RELATION . '.' . Address::STREET_RELATION,
        ]);

        $response = $this->json('GET', 'api/orders/history');
        $response->assertStatus(200);
        $response->assertJsonCount(3, 'data');

        $formatter = $this->app->make(MoneyFormatter::class);

        $contactRelation = $orders[0]->getRelation(Order::CONTACT_RELATION);
        $addressRelation = $orders[0]->getRelation(Order::ADDRESS_RELATION);
        $orderProducts = $orders[0]->getRelation(Order::PRODUCTS_RELATION);
        $product = $orderProducts[0]->getRelation(OrderProduct::PRODUCT_RELATION);
        $streetRelation = $addressRelation->getRelation(Address::STREET_RELATION);
        $response->assertJson([
            'data' => [
                [
                    'id' => $orders[0]->getKey(),
                    'user_id' => $user->getKey(),
                    'contact_id' => $orders[0]->getAttributeValue(Order::CONTACT_ID_COLUMN),
                    'contact' => [
                        'id' => $contactRelation->getKey(),
                        'email' => $contactRelation->getAttributeValue(Contact::EMAIL_COLUMN),
                        'phone' => $contactRelation->getAttributeValue(Contact::PHONE_COLUMN)
                    ],
                    'address_id' => $orders[0]->getAttributeValue(Order::ADDRESS_ID_COLUMN),
                    'address' => [
                        'id' => $addressRelation->getKey(),
                        'street_id' => $addressRelation->getAttributeValue(Address::STREET_ID_COLUMN),
                        'street' => [
                            'id' => $streetRelation->getAttributeValue(Street::ID_COLUMN),
                            'title' => $streetRelation->getAttributeValue(Street::TITLE_COLUMN)
                        ],
                        'house' => $addressRelation->getAttributeValue(Address::HOUSE_COLUMN),
                        'apartment' => $addressRelation->getAttributeValue(Address::APARTMENT_COLUMN)
                    ],
                    'products' => [
                        [
                            'id' => $orderProducts[0]->getKey(),
                            'product_id' => $orderProducts[0]->getAttributeValue(OrderProduct::PRODUCT_ID_COLUMN),
                            'product' => [
                                'id' => $product->getAttributeValue(Product::ID_COLUMN),
                                'title' => $product->getAttributeValue(Product::TITLE_COLUMN),
                            ],
                            'price' => $formatter->format($orderProducts[0]->getAttributeValue(OrderProduct::PRICE_COLUMN)),
                            'amount' => $orderProducts[0]->getAttributeValue(OrderProduct::AMOUNT_COLUMN),
                            'cost' => $formatter->format($orderProducts[0]->getCost())
                        ]
                    ]
                ]
            ]
        ]);
    }
}
