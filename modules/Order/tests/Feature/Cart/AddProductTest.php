<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Feature\Cart;

use Illuminate\Contracts\Session\Session;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Tests\TestCase;

class AddProductTest extends TestCase
{
    /** @test */
    public function addProduct()
    {
        $product = factory(Product::class)->create([
            Product::PRICE_COLUMN => new Money(101, new Currency('EUR'))
        ]);

        $session = $this->getMockBuilder(Session::class)->getMock();
        $initCart = [
            'products' => [],
            'delivery_cost' => 199
        ];
        $session->expects($this->at(0))
            ->method('get')
            ->with('cart')
            ->willReturn($initCart);

        $this->app->instance(Session::class, $session);

        $response = $this->json('PUT', 'api/orders/cart/products/' . $product->getKey(), [
            'amount' => 3
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'products' => [
                    'product-' . $product->getKey() => [
                        'id' => $product->getKey(),
                        'title' => $product->getAttributeValue(Product::TITLE_COLUMN),
                        'price' => 1.01,
                        'amount' => 3,
                        'cost' => 3.03
                    ]
                ],
                'cart_cost' => 3.03,
                'delivery_cost' => 1.99,
                'total_cost' => 5.02
            ]
        ]);
    }
}
