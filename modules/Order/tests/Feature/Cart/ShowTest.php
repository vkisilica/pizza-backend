<?php

declare(strict_types=1);

namespace Pizza\Order\Tests\Feature\Cart;

use Illuminate\Contracts\Session\Session;
use Money\Currency;
use Money\Money;
use Pizza\Catalog\Models\Product;
use Pizza\Order\Tests\TestCase;

class ShowTest extends TestCase
{
    /** @test */
    public function show()
    {
        $products = factory(Product::class, 3)->create([
            Product::PRICE_COLUMN => new Money(101, new Currency('EUR'))
        ]);

        $session = $this->getMockBuilder(Session::class)->getMock();
        $initCart = [
            'products' => [
                'product-' . $products[0]->getKey() => [
                    'id' => $products[0]->getKey(),
                    'amount' => 1
                ],
                'product-' . $products[1]->getKey() => [
                    'id' => $products[1]->getKey(),
                    'amount' => 1
                ],
                'product-' . $products[2]->getKey() => [
                    'id' => $products[2]->getKey(),
                    'amount' => 3
                ]
            ],
            'delivery_cost' => 199
        ];
        $session->expects($this->at(0))
            ->method('get')
            ->with('cart')
            ->willReturn($initCart);

        $this->app->instance(Session::class, $session);

        $response = $this->json('GET', 'api/orders/cart');
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'products' => [
                    'product-' . $products[0]->getKey() => [
                        'id' => $products[0]->getKey(),
                        'title' => $products[0]->getAttributeValue(Product::TITLE_COLUMN),
                        'price' => 1.01,
                        'amount' => 1,
                        'cost' => 1.01
                    ],
                    'product-' . $products[1]->getKey() => [
                        'id' => $products[1]->getKey(),
                        'title' => $products[1]->getAttributeValue(Product::TITLE_COLUMN),
                        'price' => 1.01,
                        'amount' => 1,
                        'cost' => 1.01
                    ],
                    'product-' . $products[2]->getKey() => [
                        'id' => $products[2]->getKey(),
                        'title' => $products[2]->getAttributeValue(Product::TITLE_COLUMN),
                        'price' => 1.01,
                        'amount' => 3,
                        'cost' => 3.03
                    ]
                ],
                'cart_cost' => 5.05,
                'delivery_cost' => 1.99,
                'total_cost' => 7.04
            ]
        ]);
    }
}
