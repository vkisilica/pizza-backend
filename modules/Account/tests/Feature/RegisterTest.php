<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @test */
    public function register()
    {
        $credentials = [
            'email' => 'admin@test.com',
            'password' => '123456',
            'password_confirmation' => '123456'
        ];

        $this->assertDatabaseMissing(User::TABLE_NAME, ['email' => 'admin@test.com']);

        $response = $this->json('POST', 'api/auth/register', $credentials);
        $response->assertJson(['status' => 'success']);

        $this->assertDatabaseHas(User::TABLE_NAME, ['email' => 'admin@test.com']);
    }

    /**
     * @test
     * @dataProvider registerFailedDataProvider
     */
    public function registerFailed($email, $password, $passwordConfirmation)
    {
        $credentials = [
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation
        ];

        $response = $this->json('POST', 'api/auth/register', $credentials);
        $response->assertStatus(422);
        $response->assertJson(['status' => 'error']);
    }

    public function registerFailedDataProvider()
    {
        return [
            [
                'admin@test.com',
                '123456',
                '1234567'
            ],[
                'admin@test.com',
                '12',
                '12'
            ],[
                'admin',
                '123456',
                '123456'
            ]
        ];
    }
}
