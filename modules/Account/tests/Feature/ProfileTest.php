<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Carbon\Carbon;
use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class ProfileTest extends TestCase
{
    /** @test */
    public function user()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $user = factory(User::class)->create([
            'email' => 'admin@test.com'
        ]);
        $this->be($user);

        $response = $this->json('GET', 'api/auth/user');
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'email' => 'admin@test.com',
                'created_at' => $now->toIso8601String(),
                'updated_at' => $now->toIso8601String(),
                'email_verified_at' => $now->toIso8601String()
            ]
        ]);
    }
}
