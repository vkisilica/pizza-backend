<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Carbon\Carbon;
use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class UpdateProfileTest extends TestCase
{
    /** @test */
    public function updateProfile()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'name' => 'Admin',
            'currency' => 'USD'
        ]);
        $this->be($user);

        $response = $this->json('PUT', 'api/auth/user', [
            'email' => 'admin2@test.com',
            'name' => 'Admin 2',
            'currency' => 'EUR'
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'email' => 'admin@test.com',
                'name' => 'Admin 2',
                'currency' => 'USD'
            ]
        ]);
    }

    /**
     * @test
     * @dataProvider updateProfileFailedFailedDataProvider
     */
    public function updateProfileFailed($name)
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'name' => 'Admin',
            'currency' => 'USD'
        ]);
        $this->be($user);

        $response = $this->json('PUT', 'api/auth/user', [
            'name' => $name
        ]);

        $response->assertStatus(422);
    }

    public function updateProfileFailedFailedDataProvider()
    {
        return [
            [
                'name' => 'A',
            ]
        ];
    }
}
