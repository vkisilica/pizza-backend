<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class LogoutTest extends TestCase
{
    /** @test */
    public function logout()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'password' => bcrypt('123456')
        ]);

        $response = $this->json('POST', 'api/auth/login', ['email' => 'admin@test.com', 'password' => '123456']);

        $response = $this->json('POST', 'api/auth/logout');
        $response->assertStatus(200);
        $response->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ]);
    }
}
