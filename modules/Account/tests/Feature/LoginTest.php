<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class LoginTest extends TestCase
{
    /** @test */
    public function login()
    {
        $this->withoutExceptionHandling();
        $credentials = [
            'email' => 'admin@test.com',
            'password' => '123456'
        ];
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'password' => bcrypt('123456')
        ]);

        $response = $this->json('POST', 'api/auth/login', $credentials);
        $response->assertStatus(200);
        $response->assertHeader('Authorization');
    }

    /** @test */
    public function loginFailedWith401StatusCode1()
    {
        $this->withoutExceptionHandling();
        $credentials = [
            'email' => 'admin@test.com',
            'password' => '1234567'
        ];
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'password' => bcrypt('123456')
        ]);

        $response = $this->json('POST', 'api/auth/login', $credentials);
        $response->assertStatus(401);
    }

    /** @test */
    public function loginFailedWith401StatusCode2()
    {
        $this->withoutExceptionHandling();
        $credentials = [
            'email' => 'admin@test.co',
            'password' => '123456'
        ];
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'password' => bcrypt('123456')
        ]);

        $response = $this->json('POST', 'api/auth/login', $credentials);
        $response->assertStatus(401);
    }
}
