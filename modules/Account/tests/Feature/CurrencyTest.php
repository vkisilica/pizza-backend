<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Session\Session;
use Pizza\Account\Http\Controllers\CurrencyController;
use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class CurrencyTest extends TestCase
{
    /** @test */
    public function authUserRetrievesCurrency()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'currency' => 'USD'
        ]);
        $this->be($user);

        $response = $this->json('GET', 'api/auth/currency');
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'currency' => 'USD'
            ]
        ]);
    }

    /** @test */
    public function guestRetrievesCurrency()
    {
        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->once())
            ->method('get')
            ->with('user.currency')
            ->willReturn('USD');

        $this->app->instance(Session::class, $session);

        $response = $this->json('GET', 'api/auth/currency');
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'currency' => 'USD'
            ]
        ]);
    }

    /** @test */
    public function guestRetrievesDefaultCurrency()
    {
        $this->withoutExceptionHandling();

        $session = $this->getMockBuilder(Session::class)->getMock();
        $session->expects($this->once())
            ->method('get')
            ->with('user.currency')
            ->willReturn(null);

        $this->app->instance(Session::class, $session);

        $response = $this->json('GET', 'api/auth/currency');
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'currency' => 'EUR'
            ]
        ]);
    }
}
