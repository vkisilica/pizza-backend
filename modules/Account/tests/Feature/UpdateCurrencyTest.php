<?php

declare(strict_types=1);

namespace Pizza\Account\Tests\Feature;

use Pizza\Account\Models\User;
use Pizza\Account\Tests\TestCase;

class UpdateCurrencyTest extends TestCase
{
    /**
     * @test
     * @dataProvider currencyDataProvider
     */
    public function authUserUpdatesCurrency($currency)
    {
        $user = factory(User::class)->create([
            'email' => 'admin@test.com',
            'name' => 'Admin',
            'currency' => 'USD'
        ]);
        $this->be($user);

        $response = $this->json('PUT', 'api/auth/currency', [
            'currency' => $currency
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'currency' => $currency
            ]
        ]);
    }

    /**
     * @test
     * @dataProvider currencyDataProvider
     */
    public function guestUpdatesCurrency($currency)
    {
        $response = $this->json('PUT', 'api/auth/currency', [
            'currency' => $currency
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'success',
            'data' => [
                'currency' => $currency
            ]
        ]);
    }

    public function currencyDataProvider()
    {
        return [
            [
                'USD'
            ],[
                'EUR'
            ]
        ];
    }
}
