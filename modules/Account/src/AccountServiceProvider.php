<?php

declare(strict_types=1);

namespace Pizza\Account;

use Illuminate\Support\ServiceProvider;
use Pizza\SharedKernel\SharedKernelServiceProvider;

class AccountServiceProvider extends ServiceProvider
{
    /** @inheritDoc */
    public function boot()
    {
        $this->loadFactoriesFrom(__DIR__ . '/../database/factories');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
    }

    /** @inheritDoc */
    public function register()
    {
        parent::register();

        $this->app->register(SharedKernelServiceProvider::class);
    }
}
