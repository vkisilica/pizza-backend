<?php

declare(strict_types=1);

namespace Pizza\Account\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const TABLE_NAME = 'account_users';

    const EMAIL_VERIFIED_AT = 'email_verified_at';

    const ID_COLUMN = 'id';
    const NAME_COLUMN = 'name';
    const EMAIL_COLUMN = 'email';
    const CURRENCY_COLUMN = 'currency';
    const PASSWORD_COLUMN = 'password';

    protected $table = self::TABLE_NAME;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME_COLUMN,
        self::EMAIL_COLUMN,
        self::CURRENCY_COLUMN,
        self::PASSWORD_COLUMN,
    ];

    protected $dates = [
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getCreatedAt(): Carbon
    {
        return $this->getAttributeValue(static::CREATED_AT);
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->getAttributeValue(static::UPDATED_AT);
    }

    public function getEmailVerifiedAt(): Carbon
    {
        return $this->getAttributeValue(static::EMAIL_VERIFIED_AT);
    }
}
