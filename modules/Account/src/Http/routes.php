<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::prefix('api')
    ->middleware('api')
    ->namespace('Pizza\Account\Http\Controllers')
    ->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('register', 'RegisterController');
        Route::post('login', 'LoginController');
        Route::get('refresh', 'RefreshController');
        Route::get('currency', 'CurrencyController');
        Route::put('currency', 'UpdateCurrencyController');

        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('user', 'ProfileController');
            Route::put('user', 'UpdateProfileController');
            Route::post('logout', 'LogoutController');
        });
    });
});
