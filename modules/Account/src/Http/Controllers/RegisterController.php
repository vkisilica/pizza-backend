<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Pizza\Account\Models\User;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email|unique:' . User::TABLE_NAME,
            'password'  => 'required|min:3|confirmed',
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }
}
