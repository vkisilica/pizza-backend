<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Pizza\Account\Http\Resources\UserResource;
use Pizza\Account\Models\User;

class UpdateProfileController extends Controller
{
    public function __invoke(Request $request, Guard $guard)
    {
        $v = Validator::make($request->all(), [
            'name' => 'min:2|max:255',
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        /** @var User $user */
        $user = $guard->user();
        $user->setAttribute(User::NAME_COLUMN, $request->get('name'));
        $user->save();

        return response()->json([
            'status' => 'success',
            'data' => UserResource::make($user),
            'meta' => $request
        ], 200);
    }
}
