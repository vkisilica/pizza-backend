<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Pizza\Account\Http\Resources\UserResource;

class ProfileController extends Controller
{
    public function __invoke(Request $request, Guard $guard)
    {
        return response()->json([
            'status' => 'success',
            'data' => UserResource::make($guard->user())
        ]);
    }
}
