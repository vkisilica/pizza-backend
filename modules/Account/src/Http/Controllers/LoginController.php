<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __invoke(Request $request, Guard $guard)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $guard->attempt($credentials)) {
            $request->session()->regenerate();
            return response()->json([
                'status' => 'success',
            ], 200)->header('Authorization', $token);
        }

        return response()->json(['error' => 'login_error'], 401);
    }
}
