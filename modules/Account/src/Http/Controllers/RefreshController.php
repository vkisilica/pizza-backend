<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;

class RefreshController extends Controller
{
    public function __invoke(Guard $guard)
    {
        if ($token = $guard->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }

        return response()->json(['error' => 'refresh_token_error'], 401);
    }
}
