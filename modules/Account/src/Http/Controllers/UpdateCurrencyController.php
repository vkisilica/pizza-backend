<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Pizza\Account\Models\User;

class UpdateCurrencyController extends Controller
{
    public function __invoke(Request $request, Guard $guard, Session $session)
    {
        $v = Validator::make($request->all(), [
            'currency' => Rule::in(['EUR', 'USD']),
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        if ($guard->check()) {
            /** @var User $user */
            $user = $guard->user();
            $user->{User::CURRENCY_COLUMN} = $request->get('currency');
            $user->save();

            $currency = $user->getAttributeValue(User::CURRENCY_COLUMN);
        } else {
            $session->put('user.currency', $request->get('currency'));
            $currency = $session->get('user.currency');
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'currency' => $currency
            ]
        ], 200);
    }
}
