<?php

declare(strict_types=1);

namespace Pizza\Account\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Pizza\Account\Models\User;

class CurrencyController extends Controller
{
    /**
     * @param Request $request
     * @param Guard $guard
     * @param Session $session
     * @param Repository $config
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request, Guard $guard, Session $session, Repository $config)
    {
        if ($guard->check()) {
            /** @var User $user */
            $user = $guard->user();
            $currency = $user->getAttributeValue(User::CURRENCY_COLUMN);
        } else {
            $currency = $session->get('user.currency');
        }
        return response()->json([
            'status' => 'success',
            'data' => [
                'currency' => $currency ?: $config->get('money.default.currency')
            ]
        ]);
    }
}
